import Todo from "./Components/Todo";

/**
 * component
 * 외부에서 사용하도록 만든, jsx코드를 리턴하는 함수
 * 반드시 결과값을 리턴
 * html 코드 처럼 보이지만 js코드기 때문에 표준 html에서 사용하는 문법이 안 먹을 수 있음
 * 브라우저에서 인식하지 못하기 때문에, 실행되면 element에 리턴된 내용을 렌더링
 * 
 */

function App() {
  return (
    <div>
      <h1>My Todos</h1>
      <Todo text="title a"/>
      <Todo text="title b"/>
      <Todo text="title c"/>
    </div>
  );
}

export default App;
