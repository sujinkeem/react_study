function Backdrop(props) {
    // 함수를 props에 담아서 전달
    return (
        <div className="backdrop" onClick={props.onCancel}></div>
    );
}

export default Backdrop;