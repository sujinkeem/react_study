import { useState } from 'react'; // react는 서드파티 라이브러리이기 때문에 경로 지정 X

import Modal from "./Modal";
import Backdrop from "./Backdrop";

/**
 * 커스텀 html element는 대문자로 시작해야 함
 * 리액트가 내부적으로 내장 html element와 커스텀 element를 구분할 때 대소문자로 구별하기 때문
 * 
 * 1. props
 * 모든 함수 컴포넌트가 받을 수 있는 매개변수
 * key:value 형식으로 전달
 * 컴포넌트가 렌더링된 곳에서 데이터를 받아옴
 * 
 * ==> 재사용 가능한 컴포넌트를 만들 때 중요
 */
function Todo(props) {
    /**
     * 3. state
     * useState
     *  - 애플리케이션에서 사용하고자 하는 상태를 등록 -> 리액트는 현재 활성화된 상태에 따라 다른 결과물을 랜더링
     *  - hook: 컴포넌트 함수나 커스텀 훅 안에서 바로 호출되어야 함
     *  - 항상 두 개의 element를 가진 배열을 리턴
     *      첫 번째 element: 현재 state의 스냅샷 - 처음에는 초기값으러 넣은 false가 할당
     *      두 번째 Element: state 값을 변경할 수 있게 하는 함수
     * 
     * ==> 화면에 보이는 내용을 다이나믹하게 바꿀 떄 중요
     */
    const [modalIsOpen, setModalIsOpen] = useState(false);

    /**
     * 2. event
     * onClick={deleteHandler()} - 괄호 넣으면 안됨. js가 리액트 코드 검증할 때 실행
     * function 안에 중첩된 함수로 생성
     * 한 군데의 todo에만 정의했는데, 각각의 todo에 대해 각기 다른 로직을 쉽게 작성할 수 있다 --> 재사용성
     */
    function deleteHandler() {
        //console.log(props.text);
        setModalIsOpen(true);
    }
    function closeModalHandler(){
        setModalIsOpen(false);
    }

    /**
     * 정적 인터페이스
     * 사용자가 어떤 인터랙션을 하든 인터페이스가 변하지 않음
     * 로딩된 페이지를 바꾸는 법 --> state
     */
    /*return (
        <div className="card">
            <h2>{props.text}</h2>
            <div className="actions">
                <button className="btn" onClick={deleteHandler}>Delete</button>
            </div>
            <Modal/>
            <Backdrop/>
        </div>
    );*/
    
    return (
        <div className="card">
            <h2>{props.text}</h2>
            <div className="actions">
                <button className="btn" onClick={deleteHandler}>Delete</button>
            </div>
            {modalIsOpen && <Modal onCancel={closeModalHandler} onConfirm={closeModalHandler}/>}
            {modalIsOpen && <Backdrop onCancel={closeModalHandler}/>}
        </div>
    );
    /**
     * <Backdrop onClick={closeModalHandler}/>
     * 커스텀 컴포넌트에서 html 표준 코드를 사용할 수 없음. onClick prop 작동 X
     */
  }

  export default Todo;