import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import App from './App';

/**
 * index.js : react 어플리케이션의 시작점. 브라우저에서 실행하는 최초의 코드
 * render 함수
 * 첫번째 인자: jsx - 브라우저에서 인식하지 못하기 때문에 내부적으로 변환
 * 두번째인자: 화면의 dom 어느 부분에 렌더링 하는지 지정
 * --> app.js 파일에 설정된 html element를 (사용자 지정 app element)를 root id를 가진 자리에 랜더링 해라
 * 
 * 리액트로 만든 spa는 서버에 html 파일이 단 하나. -> index.html
 */

ReactDOM.render(<App />, document.getElementById('root'));
