import React, {useState} from 'react';

import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense"

const DUMMY_EXPENSES = [
  { 
    id: 'e1',
    title: 'toilet paper',
    amount: '12.34',
    date: new Date(2022, 1, 4)
  },
  { 
    id: 'e2',
    title: 'new tv',
    amount: '43.23',
    date: new Date(2022, 2, 15)
  },
  { 
    id: 'e3',
    title: 'car insurance',
    amount: '523.12',
    date: new Date(2022, 3, 7)
  }
];

function App() {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseHandler = expense => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  }

  // props: 컴포넌트에 value 추가
  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler}/>
      <Expenses items={expenses}/>
    </div>
  );
}

export default App;
