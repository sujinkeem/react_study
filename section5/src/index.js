// index.js파일이 실행되는 첫 번째 파일
// index.html 싱글 html 파일의 완성된 리액트 앱 코드를 가져옴. 스크린에서 뭘 보게 될지 업데이트 하는 일을 한다.
import ReactDOM from 'react-dom';

import './index.css';
import App from './App';

// 최상위 컴포넌트(App)만이 html 페이지에 직접 생성 (리액트 돔 렌더 지시)
// 다른 하위 컴포넌트들은 html 코드 안에 html 요소로 생성
ReactDOM.render(<App />, document.getElementById('root'));
