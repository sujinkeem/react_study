import './Card.css';

// 컴포지션
function Card(props) {
    const classes = 'card ' + props.className;
    // prop children: 래퍼 컴포넌트를 만들 수 있음
    return <div className={classes}>{props.children}</div>
}

export default Card;