import ExpenseDate from './ExpenseDate';
import Card from './Card';
import './ExpenseItem.css';

// 하나의 JSX 코드 당 하나의 루트 요소만 가져야 한다
// {} 중괄호 내부에서 js 코드 인식
function ExpenseItem(props) {
    // 하나의 매개변수를 모든 컴포넌트에서 사용
    return (
        <Card className="expense-item">
            <ExpenseDate date={props.date} />
            <div className="expense-item__description">
                <h2>{props.title}</h2>
                <div className="expense-item__price">${props.amount}</div>
            </div>
        </Card>
    );
}

export default ExpenseItem;