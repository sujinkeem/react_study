import Expenses from "./components/Expenses";
// 컴포넌트: html 코드를 리턴하는 자바스크립트의 함수
// JSX: 자바스크립트 xml -> 브라우저에서 작동하는 html 코드로 자동 변환
function App() {
  const expense = [
    { 
      id: 'e1',
      title: 'toilet paper',
      amount: '12.34',
      date: new Date(2022, 1, 4)
    },
    { 
      id: 'e2',
      title: 'new tv',
      amount: '43.23',
      date: new Date(2022, 2, 15)
    },
    { 
      id: 'e3',
      title: 'car insurance',
      amount: '523.12',
      date: new Date(2022, 3, 7)
    }
  ]

  // props: 컴포넌트에 value 추가
  return (
    <div>
      <h2>Let's get started!</h2>
      <Expenses items={expense}/>
    </div>
  );
}

export default App;
