import classes from './Card.module.css';

function Card(props) {
    /*<div>를 렌더링 할 뿐, 감싸고 있는 다른 컨텐츠를 어떻게 처리할지에 대해 모름*/
    /*return (
        <div className={classes.card}>
        </div>
    );*/

    /**props.childeren: 모든 컴포넌트가 기본적으로 받아들이는 특수한 prop
     * 오프닝과 클로징 태그 사이의 컴포넌트 텍스트가 내용임
     */
    return (
        <div className={classes.card}>
            {props.children}
        </div>
    );
}

export default Card;