import classes from './NewMeetupForm.module.css';
import Card from '../ui/Card';
import { useRef } from 'react';

function NewMeetupForm(props) {
/**
 * form이 submit 되기 위해서는
 * 1. submit이 일어나는 순간을 감지 -> onSubmit 이벤트
 * 2. 부라우저가 디폹트로 http request를 보내서 페이지를 리로드 하는 것을 차단
 * 3. 입력값 가져오기
 */

    // 이 ref객체를 input태그에 연결해주면, input element로 접근이 가능해짐
    const titleInputRef = useRef();
    const imageInputRef = useRef();
    const addressInputRef = useRef();
    const descriptionInputRef = useRef();

    function submitHandler(event) {
        // 페이지 리로드를 방지하기 위해 브라우저의 기본 동작을 차단(js)
        event.preventDefault();

        // 입력값 가져오기 
        /**
         * input 태그에 onChange 이벤트 리스너를 추가해서 키를 누를 때 마다 함수를 트리거하는 방식
         * --> form이 submit 되는 순간의 값을 읽어와야 하기 때문에 부적절
         * 
         * ref : Dom element로 reference를 설정할 수 있게 해줌 -> dom element에 직접적으로 접근 가능
         */
        const enteredTitle = titleInputRef.current.value;
        const enteredImage = imageInputRef.current.value;
        const enteredAddress = addressInputRef.current.value;
        const enteredDesctipn = descriptionInputRef.current.value;

        const meetupData = {
            title: enteredTitle,
            image: enteredImage,
            address: enteredAddress,
            desctipn: enteredDesctipn
        }

        //console.log(meetupData);
        // newmeetupform을 사용하고 있는 부모 컴포넌트로 데이터를 넘김 (함수를 value로 가지는 prop 정의)
        props.onAddMeetup(meetupData);
    }

    return (
        <Card>
            <form className={classes.form} onSubmit={submitHandler}>
                <div className={classes.control}>
                    <label htmlFor="title">meetup title</label>
                    <input type="text" required id='title' ref={titleInputRef}/>
                </div>
                <div className={classes.control}>
                    <label htmlFor="image">meetup image</label>
                    <input type="url" required id='image' ref={imageInputRef}/>
                </div>
                <div className={classes.control}>
                    <label htmlFor="address">address</label>
                    <input type="text" required id='address' ref={addressInputRef}/>
                </div>
                <div className={classes.control}>
                    <label htmlFor="description">description</label>
                    <textarea id="description" required rows='5' ref={descriptionInputRef}/>
                </div>
                <div className={classes.action}>
                    <button>add meetup</button>
                </div>
            </form>
        </Card>
    );
}

export default NewMeetupForm;