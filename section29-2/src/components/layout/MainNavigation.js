import { useContext } from 'react';
import { Link } from 'react-router-dom';
/**
 * Link
 * a 태그 사용 시, 클릭할 때 마다 새로운 request를 서버로 전달
 * react-router-dom이 a태그에 클릭 리스너를 추가, 브라우저가 서버로 request 하는 것을 방지, 옮겨 가려는 경로의 url을 해석해서 브라우저의 url창을 변경.
 */

import classes from './MainNavigation.module.css';
import FavoritesContext from '../../store/favorite-context';

function MainNavigation() {
    const favoritesCtx = useContext(FavoritesContext);

    return (
        <header className={classes.header}>
            <div>React Meetups</div>
            <nav>
                <ul>
                    <li>
                        <Link to='/'>All Meetups</Link>
                    </li>
                    <li>
                        <Link to='/new-meetup'>Add new Meetup</Link>
                    </li>
                    <li>
                        <Link to='/favorites'>
                            My favorites
                            <span className={classes.badge}>{favoritesCtx.totalFavorites}</span>
                        </Link>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export default MainNavigation;