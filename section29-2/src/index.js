import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';

// css파일을 import하면, 내부적으로 빌드 프로세스가 알아서 이 css파일을 로딩되는 페이지에 추가해줌.
import './index.css';
import App from './App';
import { FavoritesContextProvider } from './store/favorite-context';

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById('root'),
    
//    <FavoritesContextProvider>
//         <BrowserRouter>
//             <App />
//         </BrowserRouter>
//     </FavoritesContextProvider>,
    //document.getElementById('root')
);
