//import {Route, Switch} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';
// route: 컴포넌트. url 내의 각기 다른 경로를 정의, 경로별로 어떤 컴포넌트를 로딩할지 결정

import AllMeetupsPage from './pages/AllMeetups';
import NewMeetupPage from './pages/NewMeetup';
import FavoritesPage from './pages/Favorites';
import MainNavigation from './components/layout/MainNavigation';
import Layout from './components/layout/Layout';

function App() {
  /**
   * 1. 라우팅
   * 라우팅 툴을 코드에 추가해서 url 변화를 감지 -> 어떤 컴포넌트를 어떤 url에 랜딩할지 인지하도록 함
   * 
  */

  /**
   * react-router-dom v6부터 Switch가 Routes 변경
   * <Routes> 자식으로는 <Route>만 가능하기 때문에 커스텀 컴포넌트를 하위 태그로 넣는게 아니라 element 속성에 할당
   * exact는 사용 X
  */
 
  /*
  return (
    <div>
      <Switch>
        <Route path='/' exact>
          <AllMeetupsPage />
        </Route>
        <Route path='/new-meetup'>
          <NewMeetupPage />
        </Route>
        <Route path='/favorites'>
          <FavoritesPage />
        </Route>
      </Switch>
    </div>
  );*/

  return (
    <Layout>
      <MainNavigation/>
      <Routes>
        <Route path='/' element={ <AllMeetupsPage /> }>
        </Route>
        <Route path='/new-meetup' element={ <NewMeetupPage /> }>
        </Route>
        <Route path='/favorites' element={ <FavoritesPage /> }>
        </Route>
      </Routes>
    </Layout>
  );
}

export default App;
