import { useState, useEffect } from 'react';
// useEffect: 특정 조건을 만족하면 일부 코드를 실행하는 hook
import MeetupList from '../components/meetups/MeetupList';

const DUMMY_DATA = [
    {
      id: 'm1',
      title: 'This is a first meetup',
      image:
        'http://res.heraldm.com/content/image/2020/04/29/20200429000196_0.jpg',
      address: 'Meetupstreet 5, 12345 Meetup City',
      description:
        'This is a first, amazing meetup which you definitely should not miss. It will be a lot of fun!',
    },
    {
      id: 'm2',
      title: 'This is a second meetup',
      image:
        'https://t1.daumcdn.net/cfile/tistory/2207573D58CFDE2704',
      address: 'Meetupstreet 5, 12345 Meetup City',
      description:
        'This is a first, amazing meetup which you definitely should not miss. It will be a lot of fun!',
    },
];
/*
// jsx element <-> object 각각 어떻게 변환시키는지
// key 속성 필수 (고유값 할당)
function AllMeetupsPage() {
    return (
        <section>
            <h1>All Meetups</h1>
            <ul>
                {DUMMY_DATA.map((meetup) => {
                    return <li key={meetup.id}>{meetup.title}</li>;
                })}
            </ul>
        </section>
    );
}
*/


/*return (
    <section>
        <h1>All Meetups</h1>
        <MeetupList meetups={DUMMY_DATA}/>
    </section>
);*/

function AllMeetupsPage() {
    /**
     * api 호출하여 결과값을 화면에 뿌림
     * 컴포넌트가 랜더링 될 때 마다 통신
     */

    const[isLoading, setIsLoading] = useState(true);
    // 단일 컴포넌트에만 영향을 미침
    const [loadedMeetups, setLoadedMeetups] = useState([]);

    /**
     * 무한루프 발생
     * useState 호출 -> 리액트가 이 컴포넌트 함수를 재실행, 재평가한 다음 업데이트된 jsx코드를 반환
     * --> useEffect 사용
     */
    // fetch(
    //     'https://react-getting-started-d67e6-default-rtdb.firebaseio.com/meetups.json'
    // ).then((response) => {
    //     return response.json(); // json도 프로미스 객체 리턴
    // }).then((data) => {
    //     /**
    //      * 데이터 처리 시 문제점
    //      * 프로미스 체인 내부에 있는데, js는 프로미스가 완료될 때 까지 기다리지 않음
    //      * async await 로 묶어 버리면 전체 컴포넌트 함수가 프로미스를 반환 -> 리액트 컴포넌트는 반드시 동기식어야아 하기 때문에 유효한 컴포넌트 X
    //      * --> 임시 jsx 코드 반환 후, 응답 후 코드 업데이트
    //      * --> state
    //      */
    //     setIsLoading(false);
    //     setLoadedMeetups(data);
    // });
    
    
    /**
     * useEffect
     * 첫번째인자: 함수 / 두번째인자: 의존형 배열
     */
    useEffect(() => {
        // 컴포넌트가 재실행될 떄 마다 실행되는 게 아니라, 특정 조건을 만족해야 실행.
        setIsLoading(true);
        fetch(
            'https://react-getting-started-d67e6-default-rtdb.firebaseio.com/meetups.json'
        ).then((response) => {
            return response.json();
        }).then((data) => {
            const meetups = [];
            for (const key in data) {
                const meetup = {
                    id: key,
                    ...data[key]
                };

                meetups.push(meetup);
            }
            
            setIsLoading(false);
            setLoadedMeetups(meetups);
        });
    }
    /**
     * 배열에 있는 값을 effect 함수가 마지막으로 실행했을 때의 값과 비교
     * (값이 비어있으면 컴포넌트가 처음으로 렌더링되고 실행될 때만 실행)
     * effect 함수가 의존하고 있는 모든 외부 값을 추가해야 함
     */
    , []);

    

    // 로딩하는 동안 문구 표기
    if (isLoading) {
        return <section>
            Loading...
        </section>;
    }

    return (
        <section>
            <h1>All Meetups</h1>
            <MeetupList meetups={loadedMeetups}/>
        </section>
    );
    // return (
    //     <section>
    //         <h1>All Meetups</h1>
    //         <MeetupList meetups={DUMMY_DATA}/>
    //     </section>
    // );
}

export default AllMeetupsPage;