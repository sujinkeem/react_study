//import { useHistory } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

import NewMeetupForm from '../components/meetups/NewMeetupForm';

function NewMeetupPage() {
    /**
     * 데이터 전송이 완료되면 다른 메뉴로 이동
     * useNavigate: 브라우저의 검색 기록을 조작하는 데 필요한 특정 메소드를 보여주는 단순한 기능
     */
    //const history = useHistory();
    const navigate = useNavigate();

    function addMeetupHandler(meetupData) {
        fetch(
            'https://react-getting-started-d67e6-default-rtdb.firebaseio.com/meetups.json',
            {
                method: 'POST',
                body: JSON.stringify(meetupData),
                header: {
                    'Content-Type': 'application/json'
                }
            }
        )
        // fetch로 가져오는 값을 promise 객체로 리턴. 리턴되면 then 내부 함수 실행.
        .then(() => {
            //history.replace('/');
            navigate('/');
        });
    }
    
    return (
        <section>
            <h1>add new meetup</h1>
            <NewMeetupForm onAddMeetup={addMeetupHandler}/>
        </section>
    );
}

export default NewMeetupPage;