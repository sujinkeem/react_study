import { createContext, useState } from 'react';

// context는 js객체 (인자: 초기값)
const FavoriteContext = createContext({
    favorites: [],
    totalFavorites: 0,
    addFavorite: (favoriteMeetup) => {},
    removeFavorite: (meetupId) => {},
    itemIsFavorite: (meetupId) => {},
});

// 값을 받으려 하는 모든 컴포넌트에 context를 제공, context 값을 업데이트
export function FavoritesContextProvider(props) {
    const [userFavorites, setUserFavorites] = useState([]);
    // provider: 해당 context와 상호 작용하는 모든 컴포넌트를 포함
    /*return (
        <FavoriteContext.Provider>
            {props.children}
        </FavoriteContext.Provider>
    );*/

    function addFavoriteHandler(favoriteMeetup) {
        setUserFavorites((prevUserFavorites) => {
            return prevUserFavorites.concat(favoriteMeetup);
        });
    }

    function removeFavoriteHandler(meetupId) {
        setUserFavorites((prevUserFavorites) => {
            return prevUserFavorites.filter(meetup => meetup.id !== meetupId);
        });
    }

    function itemIsFavoriteHandler(meetupId) {
        return userFavorites.some(meetup => meetup.id !== meetupId);
    }

    const context = {
        favorites: userFavorites,
        totalFavorites: userFavorites.length,
        addFavorite: addFavoriteHandler,
        removeFavorite: removeFavoriteHandler,
        itemIsFavorite: itemIsFavoriteHandler
    }

    return (
        <FavoriteContext.Provider value={context}>
            {props.children}
        </FavoriteContext.Provider>
    );
}

export default FavoriteContext;