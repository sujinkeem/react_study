import React, {useState} from 'react';
import Card from '../UI/Card';
import ErrorModal from '../UI/ErrorModal';
import classes from './Adduser.module.css';
import Button from '../UI/Button';

const AddUser = (props) => {
    const [enterdUsername, setEnteredUsername] = useState('');
    const [enterdAge, setEnteredAge] = useState('');
    const [error, setError] = useState();
    
    const addUserHandler = (event) => {
        event.preventDefault();
        if (enterdUsername.trim().length === 0 || enterdAge.trim().length === 0) {
            setError({
                title: 'invalid input',
                message: 'please enter a valid name and age'
            });
            return;
        }
        if (+ enterdAge < 1) {
            setError({
                title: 'invalid age',
                message: 'please enter a valid age'
            });
            return;
        }

        props.addUserHandler(enterdUsername, enterdAge);
        setEnteredUsername('');
        setEnteredAge('');
    }

    const usernameChangeHandler = (event) => {
        setEnteredUsername(event.target.value);
    }

    const ageChangeHandler = (event) => {
        setEnteredAge(event.target.value);
    }

    const errorHandler = () => {
        setError(null);
    }

    return (
        <div>
            {error && <ErrorModal title={error.title} message={error.message} onConfirm={errorHandler}/>}
            <Card className={classes.input}>
                <form onSubmit={addUserHandler}>
                    <label htmlFor="username">Username</label>
                    <input id="username" type="text" value={enterdUsername} onChange={usernameChangeHandler}/>
                    <label htmlFor="age">Age(Years)</label>
                    <input id="age" type="number" value={enterdAge} onChange={ageChangeHandler}/>
                    <Button type="submit">Add User</Button>
                </form>
            </Card>
        </div>
    );
};

export default AddUser;